---
title: "2017 Annual Report"
output: 
  flexdashboard::flex_dashboard:
    vertical_layout: scroll
    logo: img/logo.png
    favicon: img/fav.png
    orientation: rows
    self_contained: true
    navbar:
      - { title: "OIPM Home", href: "http://nolaipm.gov", align: right }
---

```{r setup, echo = FALSE, cache = FALSE, include = FALSE}
library(flexdashboard)
source("annual_report_2017.R")

# Force
#source("analysis/force/force_by_year.R")
#source("analysis/force/force_by_month.R")
#source("analysis/force/uof_by_type.R")
#source("analysis/force/uof_by_reason.R")
#source("analysis/force/uof_by_level_and_year.R")
#source("analysis/force/uof_pies_each_type.R")
#source("analysis/force/uof_by_effectiveness.R")
#source("analysis/force/uof_by_district_level.R")
#source("analysis/force/uof_by_disposition.R")
#source("analysis/force/uof_by_officer_age_exp.R")
#source("analysis/force/force_per_officer.R")
#source("analysis/force/uof_by_victim_sex_race.R")
#source("analysis/force/uof_by_officer_sex_race.R")
#source("analysis/force/uof_by_injury.R")
#source("analysis/force/uof_against_blacks_comparison.R")

# Officers
#source("analysis/officers/officer_demographics.R")
#source("analysis/officers/officer_zips.R")

# Allegations
#source("analysis/complaints/complaints_allegations_by_year.R")
#source("analysis/complaints/complaints_allegations_by_month.R")
#source("analysis/complaints/allegations_by_type.R")
#source("analysis/complaints/top_allegations.R")
#source("analysis/complaints/complaints_by_outcome.R")
#source("analysis/complaints/complaints_allegation_sex_race_disposition.R")
#source("analysis/complaints/complaints_by_disposition_officer_race.R")
#source("analysis/complaints/allegations_by_source.R")
#source("analysis/complaints/actions_taken_by_action.R")
#source("analysis/complaints/discipline_by_allegation.R")
#source("analysis/complaints/discipline_by_public_sex.R")
#source("analysis/complaints/discipline_by_race.R")
#source("analysis/complaints/complaints_by_rank_disposition.R")
#source("analysis/complaints/complaints_anonymous.R")
#source("analysis/complaints/allegations_fourth_amendment.R")
#source("analysis/complaints/allegations_retaliation.R")
#source("analysis/complaints/allegations_mediation.R")

# Outreach
#source("analysis/oipm_survey.R")

# BWC
#source("analysis/bwc/bwc.R")
```

Introduction {data-orientation=columns}
===================================== 

Column
-------------------------------------

### ATTENTION: THIS IS A DRAFT!

#### WORK IN PROGRESS
OIPM's 2017 annual report is currently a work in progress. All analysis and data contained on this website should not be relied on until the official report is
released. 

![alt text](img/wip.png "WORK IN PROGRESS")

#### NOT MOBILE FRIENDLY
For the best experience, view this website on a desktop computer using full-screen.
![alt text](img/phone.jpg "NOT MOBILE FRIENDLY")

### Navigating the report
This website is an interactive version of the statiscal analysis from OIPM's 2017 annual report Annual Report. Analysis is divided into these sections:

1. [Data Sources](#data-sources)
2. [Use of Force](#use-of-force)
3. [Complaints & Discipline](#complaints)
4. [Officer Demographics](#officers)
5. [Community outreach](#community-outreach)
6. [Body-worn camera adherence](#bodyworn-cameras)



Column
-------------------------------------
### Reproducing this report

#### tl;dr
```
git clone https://github.com/marvinmarnold/oipm_annual_report
# Install pre-requisites
Rscript run.R
```

#### Source
The data and scripts used to generate this website are available [here](https://github.com/marvinmarnold/oipm_annual_report).

#### Pre-requiites: Installing on Ubuntu 16.04

```
# Dependencies
sudo add-apt-repository -y ppa:opencpu/jq
sudo add-apt-repository -y ppa:ubuntugis/ppa
sudo apt-get update
sudo apt-get install r-base-dev r-base-core libjq-dev libcurl4-openssl-dev libssl-dev libprotobuf-dev libjq-dev libv8-3.14-dev protobuf-compiler libgdal1-dev libgdal1i libproj-dev libudunits2-dev gdal-bin python-gdal python3-gdal libgstreamer-plugins-base0.10-0 libgstreamer0.10-0

# Install pandoc 2 from http://pandoc.org/installing.html#linux

# Get newest versions 
install.packages('devtools')
library(devtools)
devtools::install_github('hadley/ggplot2')
devtools::install_github("hrecht/censusapi")
devtools::install_github('rstudio/leaflet')

# Get stable releases
install.packages(c("dplyr", "tidyr", "ggplot2", "rmarkdown", "plotly", "flexdashboard", "maps", "pandoc", "leaflet", "maptools", "geojsonio"))
```

#### Dependencies
The report was written in Rmarkdown using Rstudio and has the following dependencies that can be installed using `install.packages("LIBRARY_NAME")`:

- dplyr
- tidyr
- ggplot2
- rmarkdown
- flexdashboard
- plotly
- pandoc (read troubleshooting notes)
- maps
- devtools::install_github('rstudio/leaflet')
- geojsonio (on ubuntu, installation fails many times and instructs to install dependencies)

#### Build and run
Once you have all the dependencies installed, you can clone the repository and use Knit in Rstudio to compile `index.Rmd`
in the project's root.

#### Troubleshooting

##### Pandoc
If you have trouble with Pandoc on Linux, check out [these instructions](https://github.com/rstudio/rmarkdown/blob/master/PANDOC.md#newer-systems-debianubuntufedora).

##### geojsonio
sudo apt-get install r-base-dev r-base-core libjq-dev libcurl4-openssl-dev libssl-dev libprotobuf-dev libjq-dev libv8-3.14-dev protobuf-compiler

sudo add-apt-repository -y ppa:opencpu/jq
  sudo apt-get update
  sudo apt-get install libjq-dev libv8-3.14-dev
  
`sudo apt install libgdal1-dev libgdal1i libproj-dev libudunits2-dev` then `install.packages("rgdal", repos = "http://cran.us.r-project.org", type = "source")`. You'll probably also need to update your gdal version:

https://stackoverflow.com/questions/37294127/python-gdal-2-1-installation-on-ubuntu-16-04#41613466

Then finally: `install.packages("geojsonio")`

#### Resources
If you want to get map data for other states, check out: https://github.com/jgoodall/us-maps then convert with ogre.adc4gis.com.
Use the entire ZIP that you download, when converting, don't need to extract. (Alternative = https://github.com/OpenDataDE/State-zip-code-GeoJSON)

LA zip codes http://download.geonames.org/export/zip/

Data sources {data-orientation=rows}
===================================== 

Row 
-------------------------------------
### Repo includes all data
Clone this repo to download all data. Alternatively, use the links to download each individually.

Row 
-------------------------------------
### Active duty officers
The IAPro DB that OIPM has access to contains officer information that is not always in a useable form or up-to-date.
For the official count of active NOPD officers in 2017, we use data provided by NOPD. But for providing information about
officers involved in specific uses of force or complaints, we use information from the IAPro DB.

#### Info about officers related to each incident
- Download: TODO
- Source: OIPM direct DB access on May 18, 2018
- Issue: Does not have reliable dates of employment

#### Official count of all active duty officers in 2017
- Download: TODO
- Source: NOPD pull from ADP on May 11, 2018
- Issue: Can't cross reference with IAPro data
- Issue: Need an independent way to access this information
- Issue: Need rank over time

Row 
-------------------------------------
### Use of Force
2017 is the first year that OIPM has independent access to a copy of the database that contains force information.
We have used the data on data.nola.gov to compare with NOPD's version and cross reference with previously reported numbers.
We are unable to reproduce all of NOPD's previous figures, especially for 2015 where there is a large discrepancy.

#### Direct database access
- Download: TODO
- Source: OIPM direct DB access on May 18, 2016
- Issue: 2015 data mostly missing. 
- Issue: Was arrested field does not appear to be reliable.

#### data.nola.gov
- Download: TODO
- Issues: Does not contain a unique way to identify officers.

Row 
-------------------------------------
### Complaints

- Issue: How to identify anonymous complaints
- Issue: How to identify 4th amendment violations.
- Issue: Many things OIPM believes to be illigitimate outcomes.
- Issue: Identifying complaints made by an officer.

More coming soon

Row 
-------------------------------------
### Bodyworn cameras
Coming soon

Row 
-------------------------------------
### Community outreach
Coming soon

Row 
-------------------------------------
### Arrest

- Issue: NOPD should not have to rely on OPSO for arrest data

More coming soon


Use of force {data-orientation=rows}
===================================== 

Row 
-------------------------------------
### ATTENTION: THIS IS A DRAFT!
OIPM's 2017 annual report is currently a work in progress. All analysis and data contained on this website should not be relied on until the official report is
released. 

**WORK IN PROGRESS**
![alt text](img/wip.png "WORK IN PROGRESS")
**NOT MOBILE FRIENDLY**
For the best experience, view this website on a desktop computer using full-screen.
![alt text](img/phone.jpg "NOT MOBILE FRIENDLY")

### Police districts
```{r fig.width = 6, fig.height = 6}
p.police.districts
```

Row 
-------------------------------------
### FTN vs UOF
FTN stands for “force tracking number”. It is the designation given to track the entirety
of an interaction between NOPD and one or more individuals wherein force was used.

A single FTN corresponds to one or more UOF. If Officer A and Officer B both use their
hands against Individual C, the result would be one FTN, corresponding to two UOFs
(one for each officer). The same pattern would apply if there were multiple types of
force used or multiple individuals that force was used on.

This report will always clearly label whether FTN or UOF is being used for a particular
analysis but the onus is on the reader to remain vigilant of the distinction.

Row {data-height=300}
-------------------------------------
### Analysis
**Force by year**
Coming soon

**Force by month**
Coming soon

Row 
-------------------------------------
### Use of force by year
```{r force.by.year, fig.width = 10, fig.height = 6}
#p.force.by.year
```

### FTN & UOF in 2017 by month
```{r, fig.width = 10, fig.height = 6}
#p.force.by.month
```

Row {data-height=300}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Level 1
```{r, fig.height = 3}
#p.lvl.by.year[[1]]
```

### Level 2
```{r, fig.height = 3}
#p.lvl.by.year[[2]]
```

### Level 3
```{r, fig.height = 3}
#p.lvl.by.year[[3]]
```

### Level 4
```{r, fig.height = 3}
#p.lvl.by.year[[4]]
```

Row {data-height=300}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Force by level and type of force 
```{r, fig.width = 10, fig.height = 7}
#p.uof.by.type
```

### Force by level and district
```{r, fig.width = 10, fig.height = 7}
#p.uof.by.district.type
```

Row {data-height=300}
-------------------------------------
Coming soon

Row
-------------------------------------
### Force by type and effectiveness 
```{r, fig.width = 10, fig.height = 6}
#p.uof.by.effectiveness
```

### NOPD's determination of unauthorized force
```{r, fig.width = 10, fig.height = 4}
#p.uof.by.disposition
```


Row {data-height=450}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Level 1
```{r, fig.height = 3}
#uof.pies.each.type[[1]]
```

### Level 2
```{r, fig.height = 3}
#uof.pies.each.type[[2]]
```

### Level 3
```{r, fig.height = 3}
#uof.pies.each.type[[3]]
```

### Level 4
```{r, fig.height = 3}
#uof.pies.each.type[[4]]
```

Row {data-height=300}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Reason for force
```{r, fig.height = 3}
#p.uof.by.reason
```

### Reason for exhibiting firearms
```{r, fig.height = 3}
#p.firearm.by.reason
```

### What preceded force
```{r, fig.height = 3}
#p.uof.by.service.type
```

Row {data-height=300}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Average force per officer
```{r, fig.height = 3}
#p.force.per.officer
```

### Percentage of force my officers using most force
```{r, fig.height = 3}
#p.force.per.bucket
```

Row {data-height=100}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Contribution to FTN by top 5 officers
```{r, fig.height = 6}
#paste("They account for", top.5.ftn.pct, "% of all FTNs")
#paste(top.5.ftn.num.male, "are male")
#paste(top.5.ftn.min.age, "-", top.5.ftn.max.age, "years old")
#paste(top.5.ftn.min.exp, "-", top.5.ftn.max.exp, "years of experience")
#paste(top.5.ftn.num.white, "are white,", top.5.ftn.num.black, "are black,", top.5.ftn.num.hispanic, "are hispanic,", top.5.ftn.num.asian, "are asian,", top.5.ftn.num.native, "are native american,", top.5.ftn.num.asian, "are asian, and", top.5.ftn.num.race, "have an unknown race")
#top.5.officers.ftn.division
#top.5.officers.ftn.unit
```

### Contribution to UOF by top 5 officers
```{r, fig.height = 6}
#paste("They account for", top.5.uof.pct, "% of all UOFs")
#paste(top.5.uof.num.male, "are male")
#paste(top.5.uof.min.age, "-", top.5.uof.max.age, "years old")
#paste(top.5.uof.min.exp, "-", top.5.uof.max.exp, "years of experience")
#paste(top.5.uof.num.white, "are white,", top.5.uof.num.black, "are black,", top.5.uof.num.hispanic, "are hispanic,", top.5.uof.num.asian, "are asian,", top.5.uof.num.native, "are native american,", top.5.uof.num.asian, "are asian, and", top.5.uof.num.race, "have an unknown race")
#top.5.officers.uof.division
#top.5.officers.uof.unit
```

Row
-------------------------------------
### Contribution to FTN by top 10 officers
```{r, fig.height = 7}
#paste("They account for", top.10.ftn.pct, "% of all FTNs")
#paste(top.10.ftn.num.male, "are male")
#paste(top.10.ftn.min.age, "-", top.10.ftn.max.age, "years old")
#paste(top.10.ftn.min.exp, "-", top.10.ftn.max.exp, "years of experience")
#paste(top.10.ftn.num.white, "are white,", top.10.ftn.num.black, "are black,", top.10.ftn.num.hispanic, "are hispanic,", top.10.ftn.num.asian, "are asian,", top.10.ftn.num.native, "are native american,", top.10.ftn.num.asian, "are asian, and", top.10.ftn.num.race, "have an unknown race")
#top.10.officers.ftn.division
#top.10.officers.ftn.unit
```

### Contribution to UOF by top 10 officers
```{r, fig.height = 7}
#paste("They account for", top.10.uof.pct, "% of all UOFs")
#paste(top.10.uof.num.male, "are male")
#paste(top.10.uof.min.age, "-", top.10.uof.max.age, "years old")
#paste(top.10.uof.min.exp, "-", top.10.uof.max.exp, "years of experience")
#paste(top.10.uof.num.white, "are white,", top.10.uof.num.black, "are black,", top.10.uof.num.hispanic, "are hispanic,", top.10.uof.num.asian, "are asian,", top.10.uof.num.native, "are native american,", top.10.uof.num.asian, "are asian, and", top.10.uof.num.race, "have an unknown race")
#top.10.officers.uof.division
#top.10.officers.uof.unit
```

Row
-------------------------------------
### Contribution to FTN by top 20 officers
```{r, fig.height = 10}
#paste("They account for", top.20.ftn.pct, "% of all FTNs")
#paste(top.20.ftn.num.male, "are male")
#paste(top.20.ftn.min.age, "-", top.20.ftn.max.age, "years old")
#paste(top.20.ftn.min.exp, "-", top.20.ftn.max.exp, "years of experience")
#paste(top.20.ftn.num.white, "are white,", top.20.ftn.num.black, "are black,", top.20.ftn.num.hispanic, "are hispanic,", top.20.ftn.num.asian, "are asian,", top.20.ftn.num.native, "are native american,", top.20.ftn.num.asian, "are asian, and", top.20.ftn.num.race, "have an unknown race")
#top.20.officers.ftn.division
#top.20.officers.ftn.unit
```

### Contribution to UOF by top 20 officers
```{r, fig.height = 10}
#paste("They account for", top.20.uof.pct, "% of all UOFs")
#paste(top.20.uof.num.male, "are male")
#paste(top.20.uof.min.age, "-", top.20.uof.max.age, "years old")
#paste(top.20.uof.min.exp, "-", top.20.uof.max.exp, "years of experience")
#paste(top.20.uof.num.white, "are white,", top.20.uof.num.black, "are black,", top.20.uof.num.hispanic, "are hispanic,", top.20.uof.num.asian, "are asian,", top.20.uof.num.native, "are native american,", top.20.uof.num.asian, "are asian, and", top.20.uof.num.race, "have an unknown race")
#top.20.officers.uof.division
#top.20.officers.uof.unit
```
Row
-------------------------------------
### Force by officer age and experience 
Coming soon

```{r, fig.width = 10, fig.height = 5}
#p.uof.by.officer.age.exp
```

Row {data-height=150}
-------------------------------------
### Analysis
Coming soon

Row
-------------------------------------
### Officer injuries during UOF
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.officer.injury
```

### Victim injuries during UOF
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.victim.injury
```

Row {data-height=200}
-------------------------------------
### Analysis of UOF by victim gender and race
Coming soon

Row
-------------------------------------
### UOF by victim gender and race
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.victim.sex.race
```

### UOF by female victim race
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.female.victim.race
```

### UOF by male victim race
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.male.victim.race
```

Row
-------------------------------------
### Female victim UOF by type and race
```{r, fig.width = 5, fig.height = 5}
#p.female.victim.uof.by.type
```

### Male victim UOF by type and race
```{r, fig.width = 5, fig.height = 5}
#p.male.victim.uof.by.type
```

Row {data-height=300}
-------------------------------------
### Analysis 
Coming soon

Row
-------------------------------------
### Force used disproportionately against black people by month
```{r, fig.height = 6}
#p.black.by.month
```

### Force used disproportionately against black people by district
```{r, fig.height = 6}
#p.black.by.district
```

Row {data-height=200}
-------------------------------------
### Analysis of UOF by officer gender and race
Coming soon

Row
-------------------------------------
### UOF by officer gender and race
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.officer.sex.race
```

### UOF by female officer race
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.female.officer.race
```

### UOF by male officer race
```{r, fig.width = 5, fig.height = 4}
#p.uof.by.male.officer.race
```

Row
-------------------------------------
### Female officer UOF by type and race
```{r, fig.width = 5, fig.height = 5}
#p.female.uof.by.type
```

### Male officer UOF by type and race
```{r, fig.width = 5, fig.height = 5}
#p.male.uof.by.type
```

Complaints
=====================================

Row 
-------------------------------------
### ATTENTION: THIS IS A DRAFT!
OIPM's 2017 annual report is currently a work in progress. All analysis and data contained on this website should not be relied on until the official report is
released. 

**WORK IN PROGRESS**
![alt text](img/wip.png "WORK IN PROGRESS")
**NOT MOBILE FRIENDLY**
For the best experience, view this website on a desktop computer using full-screen.
![alt text](img/phone.jpg "NOT MOBILE FRIENDLY")

### Police districts
```{r fig.width = 6, fig.height = 6}
p.police.districts
```

Row
-------------------------------------
### Complaints vs allegation
When one or more individuals complain about an officer, a single tracking number is created. Each of these unique tracking numbers is considered a single complaint. Each complaint can contain one or more allegations of misconduct. 

Furthermore, its possible that each complaint/allegation is intiated by either a supervisor or anybody who is not their supervisor (including other officers). Complaints/allegations initiated by a superior are called "Rank Initiated" while all other complaints are "Citizen Initiated".

When a complaint/case contains more than one finding, select disposition in this order:

1. Sustained
2. Withdrawn / mediated
3. DI-2
4. Pending
5. Not sustained
6. Unfounded
7. Exonerated
8. NFIM
9. Illegitimate outcome

Row
-------------------------------------
### Complaints by year
```{r, fig.width = 5, fig.height = 3}
#p.complaints.by.year
```

### Allegations by year
```{r, fig.width = 5, fig.height = 3}
#p.allegations.by.year
```

Row
-------------------------------------
### Complaints and allegations by month
```{r, fig.width = 5, fig.height = 5}
#p.complaints.allegations.by.month
```

### Complaints by month and initiator
```{r, fig.width = 5, fig.height = 5}
#p.complaints.by.month
```

### Allegations by month and initiator
```{r, fig.width = 5, fig.height = 5}
#p.allegations.by.month
```

Row
-------------------------------------
### Most common allegations (regardless of outcome)
```{r, fig.width = 4, fig.height = 5}
#p.top.alleg
```

### Most common sustained allegations
```{r, fig.width = 4, fig.height = 5}
#p.top.sustained.alleg
```

### Most common DI-2 allegations
```{r, fig.width = 4, fig.height = 5}
#p.top.di2.alleg
```

Row
-------------------------------------
### Discipline by allegation
```{r, fig.width = 12, fig.height = 5}
#p.discipline.by.allegation
```

Row
-------------------------------------
### Rank v public complaints
```{r, fig.width = 4, fig.height = 7}
#p.complaints.by.rank.disposition
```

### Anonymous complaints
```{r, fig.width = 4, fig.height = 5}
#p.anon.allegs
```

Row
-------------------------------------
### Discipline by complainant sex
```{r, fig.width = 4, fig.height = 5}
#p.discipline.by.sex
```

### Discipline by complainant race
```{r, fig.width = 4, fig.height = 5}
#p.discipline.by.race
```

Row
-------------------------------------
### Discipline by officer sex
```{r, fig.width = 4, fig.height = 5}
#p.discipline.by.officer.sex
```

### Discipline by officer race
```{r, fig.width = 4, fig.height = 5}
#p.discipline.by.officer.race
```

Row
-------------------------------------
### Individual allegations by outcome
```{r, fig.width = 8, fig.height = 8}
#p.allegation.by.type.disposition
```

### Actions taken
```{r, fig.width = 4, fig.height = 8}
#p.actions.taken
```

Row
-------------------------------------
### All complaints by outcome
```{r, fig.width = 5, fig.height = 4}
#p.complaint.by.outcome
```

### Rank complaints by outcome
```{r, fig.width = 5, fig.height = 4}
#p.rank.complaint.by.outcome
```

### Citizen complaints by outcome
```{r, fig.width = 5, fig.height = 4}
#p.citizen.complaint.by.outcome
```

Row
-------------------------------------
### How officer race impacts disposition
```{r, fig.width = 8, fig.height = 7}
#p.allegation.race.disposition
```

### How officer race impacts disposition
```{r, fig.width = 4, fig.height = 7}
#p.complaint.disposition.by.officer.race
```

Row
-------------------------------------
### Allegations by source
```{r, fig.width = 5, fig.height = 7}
#p.alleg.by.source
```

### Fourth amendment violations
```{r, fig.width = 7, fig.height = 7}
#p.fourth.viol
```

Row
-------------------------------------
### Allegations by source
```{r, fig.width = 5, fig.height = 4}
#p.retaliation
```

### Mediation misclassifications
```{r, fig.width = 5, fig.height = 4}
#p.mediation
```


Officers
===================================== 

Row 
-------------------------------------
### ATTENTION: THIS IS A DRAFT!
OIPM's 2017 annual report is currently a work in progress. All analysis and data contained on this website should not be relied on until the official report is
released. 

**WORK IN PROGRESS**
![alt text](img/wip.png "WORK IN PROGRESS")
**NOT MOBILE FRIENDLY**
For the best experience, view this website on a desktop computer using full-screen.
![alt text](img/phone.jpg "NOT MOBILE FRIENDLY")

Row
-------------------------------------
### Total number active officers
```{r, fig.width = 5, fig.height = 5}
#p.active.officers
```

### 2017 active officers by race
```{r, fig.width = 5, fig.height = 5}
#p.officers.by.race
```

### 2017 active officers by sex
```{r, fig.width = 5, fig.height = 5}
#p.officers.by.sex
```

Row
-------------------------------------
### Where officers live by zip
```{r, fig.width = 5, fig.height = 5}
#p.officer.map
```

Community outreach {data-orientation=rows}
===================================== 

Row
-------------------------------------
### Concerned with homicide investigations
What are your greatest concerns about NOPD?
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[1]]
```

### Concerned with treatment of juveniles
What are your greatest concerns about NOPD?
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[2]]
```

### Concerned with response time
What are your greatest concerns about NOPD?
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[3]]
```

Row
-------------------------------------
### Concerned with human rights violations
What are your greatest concerns about NOPD?
```{r, fig.width = 5, fig.height = 4}
#p.survey.pies[[4]]
```

### Concerned with crime victim
What are your greatest concerns about NOPD?
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[5]]
```

### Concerned other
What are your greatest concerns about NOPD?
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[6]]
```

Row
-------------------------------------
### Prioritize bad apples
If you were the police monitor, what would your priorities be?
***Cleaning out the “bad apples” and making sure NOPD officers are punished for wrongdoing***
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[7]]
```

### Prioritize helping public
If you were the police monitor, what would your priorities be?
***Helping the public understand NOPD better and identifying areas where NOPD could improve***
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[8]]
```

### Prioritize leadership
If you were the police monitor, what would your priorities be?
***Helping NOPD leadership see and correct problems***
```{r, fig.width = 4, fig.height = 4}
#p.survey.pies[[9]]
```

Row 
-------------------------------------
### Prioritize communication of successes and failures
If you were the police monitor, what would your priorities be?
***Telling the public about all NOPD’s successes and mistakes***
```{r, fig.width = 6, fig.height = 4}
#p.survey.pies[[10]]
```

### Prioritize other
If you were the police monitor, what would your priorities be?
***Other***
```{r, fig.width = 6, fig.height = 4}
#p.survey.pies[[11]]
```

Bodyworn Cameras
===================================== 
Row 
-------------------------------------
### ATTENTION: THIS IS A DRAFT!
OIPM's 2017 annual report is currently a work in progress. All analysis and data contained on this website should not be relied on until the official report is
released later this year.

**WORK IN PROGRESS**
![alt text](img/wip.png "WORK IN PROGRESS")


Row
-------------------------------------
### Overview
NOPD's strict adherence to body-worn camera (BWC) policy is essential. In future years, OIPM hopes to 
be able to estimate BWC adherence in an automated manner that yields more accurate results.

For the time being, the closest estimate OIPM can make of NOPD's adherence to BWC policy is calculating
the number of EPR and Calls for Service that do not have a corresponding BWC metadata entry.

The methodology is detailed more below, but it is known to have inherent limitations that are likely 
causing our results to significantly underestimate NOPD's adherence to BWC policy.

#### Disclaimer
Consequently, ***OIPM does not believe that results below are representative of NOPD's true BWC adherence***. We believe this method significantly underestimates NOPD's adherence to BWC policy.

OIPM hopes that the publication of these initial results will encourage continued cooperation with NOPD so that we 
can refine our process and produce more accurate analysis in the future. 

#### Results

- 20,227 EPR are missing corresponding BWC entries, that's equivalent to 30% missing
- 186,009 Calls for Service are missing corresponding BWC entries, that's equivalent to 41% missing

### Methodology
#### BWC Meta-data
1. Use BWC metadata from data.nola.gov
2. Only include entries with date_uploaded that contains '2017'
2. Remove any BWC entries that do not contain a title or id_external in the format [letter]-[four digits]-[two digits]
3. Also retain any entries that contain multiple IDs in a single cell

#### Electronic Police Report
1. Count the total number of entries
2. Count the number of entries that can be found in BWC
3. Divide the result of #2 / #1. This is the estimated adherence

#### Calls for Service
1. Count the total number of entries
2. Count the number of entries that can be found in BWC
3. Divide the result of #2 / #1. This is the estimated adherence

Row
-------------------------------------
### EPR based results
```{r, fig.width = 6, fig.height = 4}
#p.bwc.matching.epr
```

### Call for Service based results
```{r, fig.width = 6, fig.height = 4}
#p.bwc.matching.cad
```

